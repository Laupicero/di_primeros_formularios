﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : Form
    {
        Double numActual;           // Variable que nos servirá para guardar el número actual cuando estemos pulsando números
        Double resultAcumulativo;   // Guardaremos el resultado del primer número que hayamos pulsado
        String operacionActual;     // Guardaremos la operación que hayamos plsado antes de darle al igual
        Boolean comaActivada;       // Nos servirá para evitar que introduzcamos la 'coma' de un número decimal más de una vez
        Double factor;              // Nos ayudará a partir de los números decimales para que no sume al introducir


        //Constructor
        public Calculadora()
        {
            InitializeComponent();
            this.numActual = 0.0;       
            this.resultAcumulativo = 0.0;
            tb_NumActual.Text = this.numActual.ToString();
            this.comaActivada = false;
            this.factor = 0.1;
        }

        

        // ---------------------------------------------
        // EVENTOS DE LOS BOTONES NUMÉRICOS
        // ---------------------------------------------
        private void btn1_Click(object sender, EventArgs e)
        {
            concatenarNumero(1);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            concatenarNumero(2);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            concatenarNumero(3);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            concatenarNumero(4);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            concatenarNumero(5);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            concatenarNumero(6);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            concatenarNumero(7);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            concatenarNumero(8);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            concatenarNumero(9);
            tb_NumActual.Text = numActual.ToString();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            concatenarNumero(0);
            tb_NumActual.Text = numActual.ToString();
        }

        
        //Método privado que nos ayudará a concatenar números
        private void concatenarNumero(int a)
        {
            if (comaActivada)
            {
                this.numActual = this.numActual + this.factor * a;
                this.factor *= 0.1; //Tendremos que hacer esto sino sumara los decimales y no concatenará los números
            }
            else
                this.numActual = (this.numActual * 10) + a;
        }

        // ---------------------------------------------
        // EVENTOS DE LAS OPERACIONES
        // ---------------------------------------------
        //SUMA
        private void btnSUMA_Click(object sender, EventArgs e)
        {
            //Comprobación que haremo con todos los símbolos de operaciones numéricas, para que no se guarde
            // el valor '0' como resultado
            if(tb_NumActual.Text != "0")
            {
                //Primero reseteamos a '0' el número actual y guardamos ese valor
                this.resultAcumulativo = this.numActual;
                this.numActual = 0;

            }
            tb_NumActual.Text = this.numActual.ToString();
            tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
            this.factor = 0.1;
            this.comaActivada = false;
            this.operacionActual = "suma";
        }

        
        //RESTA
        private void btnRESTA_Click(object sender, EventArgs e)
        {
            if (tb_NumActual.Text != "0")
            {
                this.resultAcumulativo = this.numActual;
                this.numActual = 0;
            }
            tb_NumActual.Text = this.numActual.ToString();
            tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
            this.factor = 0.1;
            this.comaActivada = false;
            this.operacionActual = "resta";
        }

        
        //MULTIPLICACIÓN
        private void btnMULTIP_Click(object sender, EventArgs e)
        {
            if (tb_NumActual.Text != "0")
            {
                this.resultAcumulativo = this.numActual;
                this.numActual = 0;           
            }
            tb_NumActual.Text = this.numActual.ToString();
            tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
            this.factor = 0.1;
            this.comaActivada = false;
            this.operacionActual = "multiplicacion";
        }

        
        //DIVISIÓN
        private void btnDIV_Click(object sender, EventArgs e)
        {
            if (tb_NumActual.Text != "0")
            {
                this.resultAcumulativo = this.numActual;
                this.numActual = 0; 
            }
            tb_NumActual.Text = this.numActual.ToString();
            tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
            this.factor = 0.1;
            this.comaActivada = false;
            this.operacionActual = "division";
        }

        // ---------------------------------------------
        // EVENTOS DEL RESULTADO Y EL RESETEO
        // ---------------------------------------------

        //Nos limpiará los textbox con el número actual y el resultado acumulado
        private void btnLIMPIAR_Click(object sender, EventArgs e)
        {
            this.numActual = 0.0;
            this.resultAcumulativo = 0.0;
            this.factor = 0.1;
            this.operacionActual = "";
            this.comaActivada = false;

            tb_NumActual.Clear();
            tb_ResultAcumulado.Clear();
        }

        
        // Evento del botón del resultado
        private void btnRESULT_Click(object sender, EventArgs e)
        {
            if(this.numActual != 0)
            {
                switch (this.operacionActual)
                {
                    case "suma":
                        this.resultAcumulativo += this.numActual;
                        this.numActual = 0;
                        tb_NumActual.Text = this.numActual.ToString();
                        tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
                        break;

                    case "resta":
                        this.resultAcumulativo -= this.numActual;
                        this.numActual = 0;
                        tb_NumActual.Text = this.numActual.ToString();
                        tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
                        break;

                    case "multiplicacion":
                        this.resultAcumulativo *= this.numActual;
                        this.numActual = 0;
                        tb_NumActual.Text = this.numActual.ToString();
                        tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
                        break;

                    case "division":
                        this.resultAcumulativo /= this.numActual;
                        this.numActual = 0;
                        tb_NumActual.Text = this.numActual.ToString();
                        tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
                        break;
                }
            }

            //Para evitar que rompamos la calculadora
            // Si le damos al btnResultado('=') y hay escrito números en nuestra variable/textBox
            //  Se guardará en el resultado acumulado
            if(this.resultAcumulativo == 0 && this.numActual != 0)
            {
                this.resultAcumulativo = this.numActual;
                this.numActual = 0;
                tb_NumActual.Text = this.numActual.ToString();
                tb_ResultAcumulado.Text = this.resultAcumulativo.ToString();
            }

            this.operacionActual = "";
        }// Fin método/evento click 'resultado'


        //Nos servirá para controlar la insercción de sólo una vez de la coma
        private void btnCOMA_Click(object sender, EventArgs e)
        {
            if (!comaActivada)
            {
                tb_NumActual.Text = numActual.ToString() + ",";
                this.comaActivada = true;
            }            
        }
    }
}
