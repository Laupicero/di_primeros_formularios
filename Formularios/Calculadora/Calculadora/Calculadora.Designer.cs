﻿namespace Calculadora
{
    partial class Calculadora
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculadora));
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnCOMA = new System.Windows.Forms.Button();
            this.btnDIV = new System.Windows.Forms.Button();
            this.btnMULTIP = new System.Windows.Forms.Button();
            this.btnRESTA = new System.Windows.Forms.Button();
            this.btnSUMA = new System.Windows.Forms.Button();
            this.btnRESULT = new System.Windows.Forms.Button();
            this.btnLIMPIAR = new System.Windows.Forms.Button();
            this.tb_ResultAcumulado = new System.Windows.Forms.TextBox();
            this.tb_NumActual = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(12, 65);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(52, 48);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(93, 65);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(52, 48);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(174, 65);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(52, 48);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(12, 119);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(52, 48);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(93, 119);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(52, 48);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(174, 119);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(52, 48);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(12, 173);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(52, 48);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(93, 173);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(52, 48);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(174, 173);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(52, 48);
            this.btn9.TabIndex = 8;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(12, 227);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(133, 48);
            this.btn0.TabIndex = 9;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnCOMA
            // 
            this.btnCOMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCOMA.Location = new System.Drawing.Point(174, 227);
            this.btnCOMA.Name = "btnCOMA";
            this.btnCOMA.Size = new System.Drawing.Size(52, 48);
            this.btnCOMA.TabIndex = 10;
            this.btnCOMA.Text = ",";
            this.btnCOMA.UseVisualStyleBackColor = true;
            this.btnCOMA.Click += new System.EventHandler(this.btnCOMA_Click);
            // 
            // btnDIV
            // 
            this.btnDIV.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDIV.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnDIV.Location = new System.Drawing.Point(255, 65);
            this.btnDIV.Name = "btnDIV";
            this.btnDIV.Size = new System.Drawing.Size(80, 48);
            this.btnDIV.TabIndex = 11;
            this.btnDIV.Text = "/";
            this.btnDIV.UseVisualStyleBackColor = true;
            this.btnDIV.Click += new System.EventHandler(this.btnDIV_Click);
            // 
            // btnMULTIP
            // 
            this.btnMULTIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMULTIP.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnMULTIP.Location = new System.Drawing.Point(255, 119);
            this.btnMULTIP.Name = "btnMULTIP";
            this.btnMULTIP.Size = new System.Drawing.Size(80, 48);
            this.btnMULTIP.TabIndex = 12;
            this.btnMULTIP.Text = "X";
            this.btnMULTIP.UseVisualStyleBackColor = true;
            this.btnMULTIP.Click += new System.EventHandler(this.btnMULTIP_Click);
            // 
            // btnRESTA
            // 
            this.btnRESTA.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRESTA.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnRESTA.Location = new System.Drawing.Point(255, 173);
            this.btnRESTA.Name = "btnRESTA";
            this.btnRESTA.Size = new System.Drawing.Size(80, 48);
            this.btnRESTA.TabIndex = 13;
            this.btnRESTA.Text = "-";
            this.btnRESTA.UseVisualStyleBackColor = true;
            this.btnRESTA.Click += new System.EventHandler(this.btnRESTA_Click);
            // 
            // btnSUMA
            // 
            this.btnSUMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSUMA.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnSUMA.Location = new System.Drawing.Point(255, 227);
            this.btnSUMA.Name = "btnSUMA";
            this.btnSUMA.Size = new System.Drawing.Size(80, 48);
            this.btnSUMA.TabIndex = 14;
            this.btnSUMA.Text = "+";
            this.btnSUMA.UseVisualStyleBackColor = true;
            this.btnSUMA.Click += new System.EventHandler(this.btnSUMA_Click);
            // 
            // btnRESULT
            // 
            this.btnRESULT.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRESULT.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnRESULT.Location = new System.Drawing.Point(12, 281);
            this.btnRESULT.Name = "btnRESULT";
            this.btnRESULT.Size = new System.Drawing.Size(323, 48);
            this.btnRESULT.TabIndex = 15;
            this.btnRESULT.Text = "=";
            this.btnRESULT.UseVisualStyleBackColor = true;
            this.btnRESULT.Click += new System.EventHandler(this.btnRESULT_Click);
            // 
            // btnLIMPIAR
            // 
            this.btnLIMPIAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLIMPIAR.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnLIMPIAR.Location = new System.Drawing.Point(11, 377);
            this.btnLIMPIAR.Name = "btnLIMPIAR";
            this.btnLIMPIAR.Size = new System.Drawing.Size(323, 48);
            this.btnLIMPIAR.TabIndex = 16;
            this.btnLIMPIAR.Text = "LIMPIAR";
            this.btnLIMPIAR.UseVisualStyleBackColor = true;
            this.btnLIMPIAR.Click += new System.EventHandler(this.btnLIMPIAR_Click);
            // 
            // tb_ResultAcumulado
            // 
            this.tb_ResultAcumulado.Enabled = false;
            this.tb_ResultAcumulado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ResultAcumulado.ForeColor = System.Drawing.Color.DarkBlue;
            this.tb_ResultAcumulado.Location = new System.Drawing.Point(11, 28);
            this.tb_ResultAcumulado.Name = "tb_ResultAcumulado";
            this.tb_ResultAcumulado.Size = new System.Drawing.Size(322, 31);
            this.tb_ResultAcumulado.TabIndex = 17;
            this.tb_ResultAcumulado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tb_NumActual
            // 
            this.tb_NumActual.Enabled = false;
            this.tb_NumActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_NumActual.ForeColor = System.Drawing.Color.DarkBlue;
            this.tb_NumActual.Location = new System.Drawing.Point(11, 335);
            this.tb_NumActual.Name = "tb_NumActual";
            this.tb_NumActual.Size = new System.Drawing.Size(322, 31);
            this.tb_NumActual.TabIndex = 18;
            this.tb_NumActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Calculadora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 450);
            this.Controls.Add(this.tb_NumActual);
            this.Controls.Add(this.tb_ResultAcumulado);
            this.Controls.Add(this.btnLIMPIAR);
            this.Controls.Add(this.btnRESULT);
            this.Controls.Add(this.btnSUMA);
            this.Controls.Add(this.btnRESTA);
            this.Controls.Add(this.btnMULTIP);
            this.Controls.Add(this.btnDIV);
            this.Controls.Add(this.btnCOMA);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Calculadora";
            this.Text = "C A L C U L A D O R A";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnCOMA;
        private System.Windows.Forms.Button btnDIV;
        private System.Windows.Forms.Button btnMULTIP;
        private System.Windows.Forms.Button btnRESTA;
        private System.Windows.Forms.Button btnSUMA;
        private System.Windows.Forms.Button btnRESULT;
        private System.Windows.Forms.Button btnLIMPIAR;
        private System.Windows.Forms.TextBox tb_ResultAcumulado;
        private System.Windows.Forms.TextBox tb_NumActual;
    }
}

