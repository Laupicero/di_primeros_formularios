﻿namespace Conversor_Numérco
{
    partial class PrincipalForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalForm));
            this.labelTituloConversorNumerico = new System.Windows.Forms.Label();
            this.label_Num_Decimal = new System.Windows.Forms.Label();
            this.labelBinario = new System.Windows.Forms.Label();
            this.labelOctal = new System.Windows.Forms.Label();
            this.labelHexadecimal = new System.Windows.Forms.Label();
            this.textBoxNumDecimal = new System.Windows.Forms.TextBox();
            this.textBoxBinario = new System.Windows.Forms.TextBox();
            this.textBoxOctal = new System.Windows.Forms.TextBox();
            this.textBoxHexadecimal = new System.Windows.Forms.TextBox();
            this.btnConvertir = new System.Windows.Forms.Button();
            this.pictureBoxBinario = new System.Windows.Forms.PictureBox();
            this.btnSALIR = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinario)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTituloConversorNumerico
            // 
            this.labelTituloConversorNumerico.AutoSize = true;
            this.labelTituloConversorNumerico.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTituloConversorNumerico.Location = new System.Drawing.Point(51, 30);
            this.labelTituloConversorNumerico.Name = "labelTituloConversorNumerico";
            this.labelTituloConversorNumerico.Size = new System.Drawing.Size(662, 76);
            this.labelTituloConversorNumerico.TabIndex = 0;
            this.labelTituloConversorNumerico.Text = "Conversor Numérico";
            // 
            // label_Num_Decimal
            // 
            this.label_Num_Decimal.AutoSize = true;
            this.label_Num_Decimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Num_Decimal.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label_Num_Decimal.Location = new System.Drawing.Point(60, 165);
            this.label_Num_Decimal.Name = "label_Num_Decimal";
            this.label_Num_Decimal.Size = new System.Drawing.Size(166, 24);
            this.label_Num_Decimal.TabIndex = 1;
            this.label_Num_Decimal.Text = "Número Decimal";
            // 
            // labelBinario
            // 
            this.labelBinario.AutoSize = true;
            this.labelBinario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBinario.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelBinario.Location = new System.Drawing.Point(60, 229);
            this.labelBinario.Name = "labelBinario";
            this.labelBinario.Size = new System.Drawing.Size(75, 24);
            this.labelBinario.TabIndex = 2;
            this.labelBinario.Text = "Binario";
            // 
            // labelOctal
            // 
            this.labelOctal.AutoSize = true;
            this.labelOctal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOctal.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelOctal.Location = new System.Drawing.Point(60, 297);
            this.labelOctal.Name = "labelOctal";
            this.labelOctal.Size = new System.Drawing.Size(58, 24);
            this.labelOctal.TabIndex = 3;
            this.labelOctal.Text = "Octal";
            // 
            // labelHexadecimal
            // 
            this.labelHexadecimal.AutoSize = true;
            this.labelHexadecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHexadecimal.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelHexadecimal.Location = new System.Drawing.Point(60, 363);
            this.labelHexadecimal.Name = "labelHexadecimal";
            this.labelHexadecimal.Size = new System.Drawing.Size(132, 24);
            this.labelHexadecimal.TabIndex = 4;
            this.labelHexadecimal.Text = "Hexadecimal";
            // 
            // textBoxNumDecimal
            // 
            this.textBoxNumDecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumDecimal.Location = new System.Drawing.Point(247, 160);
            this.textBoxNumDecimal.Name = "textBoxNumDecimal";
            this.textBoxNumDecimal.Size = new System.Drawing.Size(213, 29);
            this.textBoxNumDecimal.TabIndex = 5;
            // 
            // textBoxBinario
            // 
            this.textBoxBinario.Enabled = false;
            this.textBoxBinario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBinario.Location = new System.Drawing.Point(247, 229);
            this.textBoxBinario.Name = "textBoxBinario";
            this.textBoxBinario.Size = new System.Drawing.Size(213, 29);
            this.textBoxBinario.TabIndex = 6;
            // 
            // textBoxOctal
            // 
            this.textBoxOctal.Enabled = false;
            this.textBoxOctal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOctal.Location = new System.Drawing.Point(247, 292);
            this.textBoxOctal.Name = "textBoxOctal";
            this.textBoxOctal.Size = new System.Drawing.Size(213, 29);
            this.textBoxOctal.TabIndex = 7;
            // 
            // textBoxHexadecimal
            // 
            this.textBoxHexadecimal.Enabled = false;
            this.textBoxHexadecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHexadecimal.Location = new System.Drawing.Point(247, 358);
            this.textBoxHexadecimal.Name = "textBoxHexadecimal";
            this.textBoxHexadecimal.Size = new System.Drawing.Size(213, 29);
            this.textBoxHexadecimal.TabIndex = 8;
            // 
            // btnConvertir
            // 
            this.btnConvertir.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnConvertir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConvertir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnConvertir.Location = new System.Drawing.Point(64, 442);
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.Size = new System.Drawing.Size(335, 48);
            this.btnConvertir.TabIndex = 9;
            this.btnConvertir.Text = "CONVERTIR";
            this.btnConvertir.UseVisualStyleBackColor = false;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // pictureBoxBinario
            // 
            this.pictureBoxBinario.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBinario.Image")));
            this.pictureBoxBinario.Location = new System.Drawing.Point(509, 128);
            this.pictureBoxBinario.Name = "pictureBoxBinario";
            this.pictureBoxBinario.Size = new System.Drawing.Size(255, 416);
            this.pictureBoxBinario.TabIndex = 10;
            this.pictureBoxBinario.TabStop = false;
            // 
            // btnSALIR
            // 
            this.btnSALIR.BackColor = System.Drawing.Color.Green;
            this.btnSALIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSALIR.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSALIR.Location = new System.Drawing.Point(64, 496);
            this.btnSALIR.Name = "btnSALIR";
            this.btnSALIR.Size = new System.Drawing.Size(335, 48);
            this.btnSALIR.TabIndex = 11;
            this.btnSALIR.Text = "SALIR";
            this.btnSALIR.UseVisualStyleBackColor = false;
            this.btnSALIR.Click += new System.EventHandler(this.btnSALIR_Click);
            // 
            // PrincipalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 565);
            this.Controls.Add(this.btnSALIR);
            this.Controls.Add(this.pictureBoxBinario);
            this.Controls.Add(this.btnConvertir);
            this.Controls.Add(this.textBoxHexadecimal);
            this.Controls.Add(this.textBoxOctal);
            this.Controls.Add(this.textBoxBinario);
            this.Controls.Add(this.textBoxNumDecimal);
            this.Controls.Add(this.labelHexadecimal);
            this.Controls.Add(this.labelOctal);
            this.Controls.Add(this.labelBinario);
            this.Controls.Add(this.label_Num_Decimal);
            this.Controls.Add(this.labelTituloConversorNumerico);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrincipalForm";
            this.Text = "Conversor de tipos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTituloConversorNumerico;
        private System.Windows.Forms.Label label_Num_Decimal;
        private System.Windows.Forms.Label labelBinario;
        private System.Windows.Forms.Label labelOctal;
        private System.Windows.Forms.Label labelHexadecimal;
        private System.Windows.Forms.TextBox textBoxNumDecimal;
        private System.Windows.Forms.TextBox textBoxBinario;
        private System.Windows.Forms.TextBox textBoxOctal;
        private System.Windows.Forms.TextBox textBoxHexadecimal;
        private System.Windows.Forms.Button btnConvertir;
        private System.Windows.Forms.PictureBox pictureBoxBinario;
        private System.Windows.Forms.Button btnSALIR;
    }
}

