﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Conversor_Numérco
{
    public partial class PrincipalForm : Form
    {
        public PrincipalForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Botón que nos realizará conversión de nuestro número en Binario, Octal y Hexadecimal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConvertir_Click(object sender, EventArgs e)
        {
            //Comprobamos que el 'textBoxNumDecimal' no esté vacío
            if (textBoxNumDecimal.TextLength >= 0 && textBoxNumDecimal.Text != null)
            {
                //Comprobación numérica. Nos aseguramos de que hemos insertado un número
                Boolean inputDataNumero = true;
                int numeroDecimal = 0;
                inputDataNumero = Int32.TryParse(textBoxNumDecimal.Text, out numeroDecimal);

                if (inputDataNumero)
                {
                    textBoxBinario.Text = conversion(numeroDecimal, 2);
                    textBoxOctal.Text = conversion(numeroDecimal, 8);
                    textBoxHexadecimal.Text = conversion(numeroDecimal, 16);
                }
                else
                {
                    borrarCasillas();
                    MessageBox.Show("¡RELLENE CON UN NÚMERO VÁLIDO!" +
                        "\nAbstengáse letras y números con decimales.", "¡ERROR!");
                }
            }
            else
            {
                MessageBox.Show("¡RELLENE PRIMERO LA CASILLA CON EL NÚMERO DECIMAL!" +
                        "\nEvite volver a comerter ese mismo error.", "¡ERROR!");
            }

        }//Fin btnCovertirClick


        //Botón SALIR
        private void btnSALIR_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Método que nos convertira nuestro número decimal a binario, octal o decimal, dependiendo
        /// de los parámetros que le pasemos
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="baseNumero"></param>
        /// <returns></returns>
        public static string conversion(int num, int conversion)
        {
            string[] letras = { "A", "B", "C", "D", "E", "F" };
            string resultado = "";
            int resto;
            while (num >= conversion)
            {
                resto = num % conversion;
                resultado = resto > 9 ? letras[resto - 10] + resultado : Convert.ToString(resto) + resultado;
                num = num / conversion;
            }
            resultado = Convert.ToString(num) + resultado;
            return resultado;
        } 

        /// <summary>
        /// Método que al invocarlo nos borrará todas las casillas numéricas
        /// Podamos escribir o no en ellas
        /// </summary>
        private void borrarCasillas()
        {
            textBoxNumDecimal.Clear();
            textBoxBinario.Clear();
            textBoxOctal.Clear();
            textBoxHexadecimal.Clear();
        }

        
    }
}
